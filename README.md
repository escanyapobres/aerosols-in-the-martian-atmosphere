# Aerosols in the martian atmosphere
_English version, there is a Catalan version below. - Versió en anglés, hi ha una versió en català més avall._

**I'm still working on this project. - Seguisc treballant en este projecte.**

## Abstract
This brief study aims to study the behavior of the dust optical depth over the years on Mars. The data used for this study have been downloaded from [The Mars Climate Database](http://www-mars.lmd.jussieu.fr/).

## Functionality
The attached code currently has the following features:
- Upload data from the Mars Climate Database.
- Show basic graphics and export them.
- Calculation of averages.
- Future: Make animations, study trends.

## Results
Average of the Dust Optical Depth for the 34th martian year:   **Falta posar que comprén entre 2018 i 2019**

![Average of the Dust Optical Depth for the 34th martian year](imgs/Dust.png)

---

## Resum
Este breu estudi té com a finalitat estudiar el comportament de l'espesor òptic de la pols al llarg dels anys a Mart. Les dades emprades per a este estudi han estat descarregades de la [Mars Climate Database](http://www-mars.lmd.jussieu.fr/).

## Funcions del codi
El codi adjunt té les següents funcions per el moment:
- Carregar dades de la Mars Climate Database.
- Mostrar gràfics bàsics i exportar-los.
- Càlcul de mitjanes.
- Futur: Realitzar animacions, estudiar tendències.

## Resultats
Mitjana del DOD per a l'any 34 marcià:  **Falta posar que comprén entre 2018 i 2019**

![Mitjana del DOD per a l'any 34 marcià](imgs/Dust.png)
